
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo assets_url('admin/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo assets_url('admin/vendor/tether/tether.min.js') ?>"></script>
    <script src="<?php echo assets_url('admin/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo assets_url('admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>
    <script src="<?php echo assets_url('admin/vendor/chart.js/Chart.min.js') ?>"></script>
    <script src="<?php echo assets_url('admin/vendor/datatables/jquery.dataTables.js') ?>"></script>
    <script src="<?php echo assets_url('admin/vendor/datatables/dataTables.bootstrap4.js') ?>"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo assets_url('admin/js/sb-admin.min.js') ?>"></script>

</body>

</html>